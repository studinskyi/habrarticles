package com.std.habrarticles.models;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HabrItem {
    private Long id;
    private String title;
    private String description;
    private Date pubDate;
    private String link;
    private String img;
    private String creator;
    private String category;

    public static ArrayList<HabrItem> listRss;

    public HabrItem(String title, String description, Date pubDate, String link, String img, String creator , String category) {
        this.id = System.currentTimeMillis() + (int) (Math.random() * 100);
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
        this.link = link;
        this.img = img;
        this.creator = creator;
        this.category = category;
    }

    @Override
    public boolean equals(Object obj) {
        //return super.equals(obj);
        if (obj == null)
            return false;

        if (!(obj instanceof HabrItem))
            return false;

        if (this.id != ((HabrItem) obj).getId()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (21 + id * 41);
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getImg() {
        return img;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd - hh:mm:ss");
        String result = getTitle() + ", автор " + this.creator + "  ( " + sdf.format(this.getPubDate()) + " )";
        return result;
    }
}
