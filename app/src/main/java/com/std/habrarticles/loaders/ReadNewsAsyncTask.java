package com.std.habrarticles.loaders;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.std.habrarticles.R;
import com.std.habrarticles.adapters.RVNewsAdapter;
import com.std.habrarticles.models.HabrItem;
import com.std.habrarticles.screens.articles.ArticlesActivity;
import com.std.habrarticles.utils.Utils;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class ReadNewsAsyncTask extends AsyncTask<String, Integer, ArrayList<HabrItem>> {
    String feedUrl;
    Context context;

    public ReadNewsAsyncTask(Context context) {
        this.context = context;
    }

    @Override
    protected ArrayList<HabrItem> doInBackground(String... params) {
        feedUrl = params[0];
        ArrayList<HabrItem> habrItems = new ArrayList<>();

        try {
            //формируем Url адрес и подключаемся к серверу RSS рассылки новостей
            URL url = null;
            try {
                url = new URL(feedUrl);
            } catch (MalformedURLException e) {
                Log.d(Utils.logTAG, "Ошибка формирования URL обхъекта - ReadNewsAsyncTask ; " + e.getMessage());
                Utils.showToast(context, "Ошибка формирования URL обхъекта - ReadNewsAsyncTask ; " + e.getMessage());
            }

            HttpURLConnection httpConnect = null;
            try {
                httpConnect = (HttpURLConnection) url.openConnection();
                httpConnect.setRequestMethod("GET");
            } catch (IOException e) {
                Log.d(Utils.logTAG, "Ошибка открытия соединения с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
                Utils.showToast(context, "Ошибка открытия соединения с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
            }

            if (httpConnect.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStream inputStream = httpConnect.getInputStream();

                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                Document document = db.parse(inputStream);
                Element element = document.getDocumentElement();

                NodeList nodeList = element.getElementsByTagName("item");
                if (nodeList.getLength() > 0) {
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        //publishProgress(i);
                        Element entry = (Element) nodeList.item(i);

                        Element _titleE = (Element) entry.getElementsByTagName("title").item(0);
                        Element _descriptionE = (Element) entry.getElementsByTagName("description").item(0);
                        Element _pubDateE = (Element) entry.getElementsByTagName("pubDate").item(0);
                        Element _creatorE = (Element) entry.getElementsByTagName("dc:creator").item(0);
                        Element _linkE = (Element) entry.getElementsByTagName("link").item(0);

                        String titleStr = getDataStringFromCDATA(_titleE);
                        String descriptionStr = getDataStringFromCDATA(_descriptionE);

                        Date pubDate = new Date(_pubDateE.getFirstChild().getNodeValue());
                        String linkStr = _linkE.getFirstChild().getNodeValue();
                        String creatorStr = _creatorE.getFirstChild().getNodeValue();
                        String categoryStr = getCategories(entry, "category");
                        String imgStr = "";

                        HabrItem newRssArticle = new HabrItem(titleStr, descriptionStr, pubDate, linkStr, imgStr, creatorStr, categoryStr);

                        habrItems.add(newRssArticle);
                    }
                }
            }
        } catch (IOException e) {
            Log.d(Utils.logTAG, "Ошибка IOException при работе с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
            Utils.showToast(context, "Ошибка IOException при работе с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
        } catch (ParserConfigurationException e) {
            Log.d(Utils.logTAG, "Ошибка ParserConfigurationException при работе с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
            Utils.showToast(context, "Ошибка ParserConfigurationException при работе с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
        } catch (SAXException e) {
            Log.d(Utils.logTAG, "Ошибка SAXException при работе с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
            Utils.showToast(context, "Ошибка SAXException при работе с RSS сервером - ReadNewsAsyncTask ; " + e.getMessage());
        }

        return habrItems;
    }

    public String getDataStringFromCDATA(Element nodeElemen) {
        String strData = "";
        NodeList nodeList = nodeElemen.getChildNodes();
        for (int index = 0; index < nodeList.getLength(); index++) {
            if (nodeList.item(index) instanceof CharacterData) {
                CharacterData child = (CharacterData) nodeList.item(index);
                strData = child.getData();

                if (strData != null && strData.trim().length() > 0) {
                    strData = child.getData();
                    break;
                }
            }
        }

        return strData;
    }

    public String getCategories(Element nodeElemen, String nameTag) {
        String strCategory = "";
        NodeList nodeList = nodeElemen.getElementsByTagName(nameTag);
        if (nodeList.getLength() > 0) {
            for (int index = 0; index < nodeList.getLength(); index++) {
                //получаем каждую сущность
                Element entry = (Element) nodeList.item(index);
                strCategory = strCategory + "/" + getDataStringFromCDATA(entry);
            }
        }
        return strCategory;
    }

    @Override
    protected void onPostExecute(ArrayList<HabrItem> habrItems) {
        super.onPostExecute(habrItems);
        // возвращение результирующего списка в адаптер списка
        ((RecyclerView) ((ArticlesActivity) context).findViewById(R.id.rvArticlesList))
                .setAdapter(new RVNewsAdapter(habrItems));
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (context != null)
            Utils.showToast(context, "load articles: " + values[0]);
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
