package com.std.habrarticles.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.std.habrarticles.R;
import com.std.habrarticles.models.HabrItem;
import com.std.habrarticles.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class RVNewsAdapter extends RecyclerView.Adapter<RVNewsAdapter.NewsHolder> {
    private List<HabrItem> listArticles = new ArrayList();

    public RVNewsAdapter(List<HabrItem> mListNews) {
        this.listArticles = mListNews;
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_list_item, parent, false);
        //NewsHolder nh = new NewsHolder(v);
        return new NewsHolder(v);
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        holder.tvTitleNews.setText(listArticles.get(position).getTitle());
        //holder.tvDescriptionArticle.setText(listArticles.get(position).getDescription());
        holder.tvLinkArticle.setText(listArticles.get(position).getLink());
        holder.tvCategoryArticle.setText(listArticles.get(position).getCategory());
        String authorPubDate = "автор - " + listArticles.get(position).getCreator() + " " +
                Utils.getFullDate(listArticles.get(position).getPubDate().getTime());
        holder.tvPubDate.setText(authorPubDate);

        // отображение элемента статьи
        WebSettings webSettings = holder.wvArticle.getSettings();
        webSettings.setJavaScriptEnabled(true);
        //holder.wvArticle.loadUrl(listArticles.get(position).getTitle());
        holder.wvArticle.loadDataWithBaseURL(null, listArticles.get(position).getDescription(), "text/html", "utf-8", null);
    }

    @Override
    public int getItemCount() {
        return listArticles.size();
    }

    class NewsHolder extends RecyclerView.ViewHolder {
        WebView wvArticle;
        ImageView ivNewsImage;
        TextView tvTitleNews;
        //TextView tvDescriptionArticle;
        TextView tvLinkArticle;
        TextView tvCategoryArticle;
        TextView tvPubDate;

        private ActionMode mActionMode;

        public NewsHolder(final View itemView) {
            super(itemView);
            wvArticle = (WebView) itemView.findViewById(R.id.wvArticle);
            ivNewsImage = (ImageView) itemView.findViewById(R.id.ivNewsImage);
            tvTitleNews = (TextView) itemView.findViewById(R.id.tvTitleNews);
            //tvDescriptionArticle = (TextView) itemView.findViewById(R.id.tvDescriptionArticle);
            tvLinkArticle = (TextView) itemView.findViewById(R.id.tvLinkArticle);
            tvCategoryArticle = (TextView) itemView.findViewById(R.id.tvCategoryArticle);
            tvPubDate = (TextView) itemView.findViewById(R.id.tvPubDate);

            // вызов контекстного меню через "Использование режима контекстных действий"
            final ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.menu_news_list, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    Utils.showToast(itemView.getContext(), "выбрана статья " + listArticles.get(getAdapterPosition()).getTitle());
                    switch (item.getItemId()) {
                        case R.id.miOpenSite:
                            int pos = getAdapterPosition();
                            String choiceURL = listArticles.get(pos).getLink();
                            Intent intentURL = new Intent(Intent.ACTION_VIEW, Uri.parse(choiceURL));
                            itemView.getContext().startActivity(intentURL);
                            mode.finish();
                            return true;
                        default:
                            return false;
                    }
                }

                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    mActionMode = null;
                }
            };

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                // Called when the user long-clicks on someView
                public boolean onLongClick(View view) {
                    if (mActionMode != null) {
                        return false;
                    }

                    mActionMode = ((Activity) itemView.getContext()).startActionMode(mActionModeCallback);
                    view.setSelected(true);
                    return true;
                }
            });

            //            // установка кликабельности картинки
            //            ivNewsImage.setOnClickListener(new View.OnClickListener() {
            //                @Override
            //                public void onClick(View v) {
            //                    Intent intentImage = new Intent(itemView.getContext(), ZoomActivity.class);
            //                    intentImage.putExtra("img",listArticles.get(getAdapterPosition()).getImg());
            //                    itemView.getContext().startActivity(intentImage);
            //                }
            //            });
        }
    }
}
