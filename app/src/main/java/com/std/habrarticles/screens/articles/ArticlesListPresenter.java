package com.std.habrarticles.screens.articles;

import android.content.Context;
import android.support.annotation.NonNull;

public class ArticlesListPresenter {
    private final IArticlesView mIArticlesView;
    private final Context context;

    public ArticlesListPresenter(@NonNull IArticlesView mIArticlesView, @NonNull Context context) {
        this.mIArticlesView = mIArticlesView;
        this.context = context;
    }

    public void loadArticles() {
        //Utils.showToast(context, "Инициализация ArticlesListPresenter");
        mIArticlesView.startAsyncTask_LoadArticles();
    }


}
