package com.std.habrarticles.screens;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.std.habrarticles.R;
import com.std.habrarticles.screens.articles.ArticlesActivity;

public class MainActivity extends AppCompatActivity {

    Button btnOpenNews;
    EditText rssURLTV;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        btnOpenNews = (Button) findViewById(R.id.btnOpenNews);
        rssURLTV = (EditText) findViewById(R.id.rssURLTV);
        rssURLTV.setText("https://habrahabr.ru/rss/hubs/all/");
    }

    public void clickOpenNews(View view) {
        String feedUrl = rssURLTV.getText().toString();

        Intent intentArticlesList = new Intent(context, ArticlesActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentArticlesList.putExtra("feedUrl", feedUrl); // передаю на активити адрес RSS рассылки новостей
        startActivity(intentArticlesList);
    }
}
