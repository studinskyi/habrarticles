package com.std.habrarticles.screens.articles;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.std.habrarticles.R;
import com.std.habrarticles.loaders.ReadNewsAsyncTask;
import com.std.habrarticles.models.HabrItem;

import java.util.ArrayList;

public class ArticlesActivity extends AppCompatActivity implements IArticlesView {
    private RecyclerView rvArticlesList;
    private static ArrayList<HabrItem> listNews = new ArrayList<>();
    private String feedUrl;
    private Context context;
    private ReadNewsAsyncTask task;

    private ArticlesListPresenter articlesPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.articles_activity);
        context = this;
        // получение с основной формы параметра Url-адреса подключения к серверу RSS рассылки новостей
        Intent intent = getIntent();
        feedUrl = intent.getStringExtra("feedUrl");

        rvArticlesList = (RecyclerView) findViewById(R.id.rvArticlesList);
        rvArticlesList.setHasFixedSize(true);
        LinearLayoutManager linLM = new LinearLayoutManager(this);
        rvArticlesList.setLayoutManager(linLM);

        // объявление и инициализация презентера
        ArticlesListPresenter articlesPresenter = new ArticlesListPresenter(this, context);
        articlesPresenter.loadArticles();
        //startAsyncTask_LoadArticles();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //        filterListNews();
        //        adapter_RV.notifyDataSetChanged();
    }

    @Override
    public void startAsyncTask_LoadArticles() {
        task = (ReadNewsAsyncTask) getLastNonConfigurationInstance();
        if (task == null)
            task = new ReadNewsAsyncTask(this);
        else
            task.setContext(this);

        task.execute(feedUrl);
    }

    public static ArrayList<HabrItem> getListNews() {
        return listNews;
    }

    public static void setListNews(ArrayList<HabrItem> listNews) {
        ArticlesActivity.listNews = listNews;
    }

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(String feedUrl) {
        this.feedUrl = feedUrl;
    }
}
